
ICON_PATH := A_ScriptDir . "\Icons\menu-4-multi-size.ico"
Menu TRAY, Icon, %ICON_PATH%


Width :=4, Height := 40
xPos := A_ScreenWidth - Width, yPos := A_ScreenHeight - Height
ToggleWidth := 3, ToggleHeight := 8
timeOn = 1000
speed = 8
BackgroundColor := 0x00111111
ForegroundColorEnabled := 0xffFFFFFF
ForegroundColorDisabled := 0x00000000



SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
#Include, Gdip.ahk
#SingleInstance, Force
#NoEnv
SetBatchLines, -1

; Start gdi+
If !pToken := Gdip_Startup()
{
	MsgBox, 48, gdiplus error!, Gdiplus failed to start. Please ensure you have gdiplus on your system
	ExitApp
}
OnExit, Exit





Gui, 1: -Caption +E0x80000 +LastFound +ToolWindow +OwnDialogs ;+AlwaysOnTop 		; Create a layered window (+E0x80000 : must be used for UpdateLayeredWindow to work!) that is always on top (+AlwaysOnTop), has no taskbar entry or caption
Gui, 1: Show, NA																; Show the window
hwnd1 := WinExist()																; Get a handle to this window we have created in order to update it later

hbm := CreateDIBSection(Width, Height)					; Create a gdi bitmap with width and height of what we are going to draw into it. This is the entire drawing area for everything
hdc := CreateCompatibleDC()								; Get a device context compatible with the screen
obm := SelectObject(hdc, hbm)							; Select the bitmap into the device context
G := Gdip_GraphicsFromHDC(hdc)							; Get a pointer to the graphics of the bitmap, for use with drawing functions
;Gdip_SetSmoothingMode(G, 4)


;;;;; create background
pBrushEnabled := Gdip_BrushCreateSolid(BackgroundColor)
Gdip_FillRoundedRectangle(G, pBrushEnabled, 0, 0, Width, Height, 0)
Gdip_DeleteBrush(pBrushEnabled)

gosub, reloadLED


;SelectObject(hdc, obm)		; Select the object back into the hdc
;DeleteObject(hbm)			; Now the bitmap may be deleted
;DeleteDC(hdc)				; Also the device context related to the bitmap may be deleted
;Gdip_DeleteGraphics(G)		; The graphics may now be deleted


~$NumLock::
~$CapsLock::
~$CapsLock up::
~$ScrollLock::
;	send {%A_ThisHotkey% d}
;	KeyWait, %A_ThisHotkey%
	gosub, reloadLED 
return

Return

;#######################################################################
	
reloadLED: 
	Gui, 1: Show, NA

	NumState := GetKeyState("NumLock", "T")
	CapsState := GetKeyState("CapsLock", "T")
	ScrollState := GetKeyState("ScrollLock", "T")


;	pBrushEnabled := Gdip_BrushCreateSolid(0xff99CCFF) 
	pBrushEnabled := Gdip_BrushCreateSolid(ForegroundColorEnabled) 
;	pBrushDisabled := Gdip_BrushCreateSolid(0xff333333) 
	pBrushDisabled := Gdip_BrushCreateSolid(ForegroundColorDisabled) 
	Gdip_FillRectangle(G, ((NumState    = 1) ? (pBrushEnabled) : (pBrushDisabled)), (Width-ToggleWidth)/2, Height/100*20-ToggleHeight/2, ToggleWidth, ToggleHeight)
	Gdip_FillRectangle(G, ((CapsState   = 1) ? (pBrushEnabled) : (pBrushDisabled)), (Width-ToggleWidth)/2, Height/100*50-ToggleHeight/2, ToggleWidth, ToggleHeight)
	Gdip_FillRectangle(G, ((ScrollState = 1) ? (pBrushEnabled) : (pBrushDisabled)), (Width-ToggleWidth)/2, Height/100*80-ToggleHeight/2, ToggleWidth, ToggleHeight)
	Gdip_DeleteBrush(pBrushEnabled)
	Gdip_DeleteBrush(pBrushDisabled)

	UpdateLayeredWindow(hwnd1, hdc, xPos, yPos, Width, Height)

Return 

;#######################################################################


;#######################################################################

Exit:
	Gdip_Shutdown(pToken)
	ExitApp
Return