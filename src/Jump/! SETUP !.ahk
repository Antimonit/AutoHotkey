SetWorkingDir %A_ScriptDir%


InputBox, browserPathIn, Setup Jump, There are 2 things you need to do to set up Jump.`r`rFirst`, specify the full path to your internet browser in the box below. To make sure it's correct`, this script will run the path after you click ok.

run %browserpathin%, UseErrorLevel

if (ErrorLevel != 0) {
	msgbox there was an error launching the path`r`r%browserpathin%
	exitApp
}

IniWrite, %browserpathIn%, Settings.ini, paths, browserPath

if (ErrorLevel != 1)
	msgbox Your browserPath has been stored in Settings.ini.`r`r"%browserpathin%"`r`r`rNow the only thing you have left to do is set up your own hotkeys to launch Jump. I recommend "!``" and "CapsLock & Enter". (These hotkeys are in the file "run in background.ahk", if you want to just run that instead of setting it up yourself.)
else {
	msgbox there was some kind of error....
	exitApp
}

