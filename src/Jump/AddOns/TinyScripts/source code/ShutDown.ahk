if 1 = sleep
{
	msgbox,4,, sleep?
	ifmsgbox yes
		DllCall("PowrProf\SetSuspendState", "int", 0, "int", 0, "int", 0)
}
else if 1 = hibernate
{	
	msgbox,4,, hibernate?
	ifmsgbox yes
		DllCall("PowrProf\SetSuspendState", "int", 1, "int", 0, "int", 0)
}
else  ;shutdown
{	
	msgbox,20,,Shut Down?
	ifMsgbox yes
	{
		Progress, p100 w354 fs10 fm30 wm300 CW800000 CTFFFF00  , ,SHUT IT DOWN!!!,,AR DARLING
		sleep 1000
		ShutDown, 1+8
	} else {
		Progress, m2 b fs11 zh0 , shutdown aborted, , , Courier New
		sleep 1000
		Progress, off
	}
}