SetTitleMatchMode RegEx ;
; Stuff to do when Windows Explorer is open
;
#IfWinActive ahk_class ExploreWClass|CabinetWClass

    ^+t::
        NewTextFile()
	return

    ; create new text file
    ^!t::Send !fhw{up

    ; open 'cmd' in the current directory
    ^!c::
        OpenCmdInCurrent()
    return
	
#IfWinActive


; Opens the command shell 'cmd' in the directory browsed in Explorer.
; Note: expecting to be run when the active window is Explorer.
;
OpenCmdInCurrent() {
    WinGetText, full_path, A  ; This is required to get the full path of the file from the address bar

    ; Split on newline (`n)
    StringSplit, word_array, full_path, `n
    full_path = %word_array1%   ; Take the first element from the array

    ; Just in case - remove all carriage returns (`r)
    StringReplace, full_path, full_path, `r, , all  
    full_path := RegExReplace(full_path, "^Address: ", "") ;

    IfInString full_path, \
    {
        Run, cmd /K cd /D "%full_path%"
    }
    else
    {
        Run, cmd /K cd /D "C:\ "
    }
}


NewTextFile() {
    WinGetText, full_path, A
    StringSplit, word_array, full_path, `n
    Loop, %word_array0%
    {
        IfInString, word_array%A_Index%, Address
        {
            full_path := word_array%A_Index%
            break
        }
    } 
    full_path := RegExReplace(full_path, "^Address: ", "")
    StringReplace, full_path, full_path, `r, , all

    IfInString full_path, \
    {
        NoFile = 0
        Loop
        {
            IfExist  %full_path%\NewTextFile%NoFile%.txt
                    NoFile++
                else
                    break
        }
        FileAppend, ,%full_path%\NewTextFile%NoFile%.txt
    }
    else
    {
        return
    }
}

^!n::

; get full path from open windows
WinGetText, FullPath, A
 
; split up result (returns paths seperated by newlines [also lame])
StringSplit, PathArray, FullPath, `n
 
; get first item
FullPath = %PathArray1%
 
; clean up result
FullPath := RegExReplace(FullPath, "(^Address: )", "")
StringReplace, FullPath, FullPath, `r, , all
 
; change working directory
SetWorkingDir, %FullPath%
 
; an error occurred with the SetWorkingDir directive
if ErrorLevel
	return
 
; display input box for file name
InputBox, UserInput, New File (example: foo.txt), , ,400, 100
 
; user pressed cancel
if ErrorLevel
    return
 
; success! output file with user input
else
	FileAppend, ,%UserInput%	
return