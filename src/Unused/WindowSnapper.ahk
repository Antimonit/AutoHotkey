;; Window Snapper
;
; AutoHotkey Version: 1.1.13.01
; Language:       English
; Platform:       Windows 7, 64 bit
; Author:         J. F. Johansen
; Website:        blog.metalight.net
; Licence:        http://creativecommons.org/licenses/by/4.0/
;
;	Function: Miscellaneous tasks

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#SingleInstance force
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
;SetTitleMatchMode 1
CoordMode, Mouse, Screen
SetWinDelay, -1

/*
Many programs have a nice dividers in their GUI - and you can usually drag the divider to resize the panels on either side. I've often been a little frustrated when resizing windows to tile them to fill the screen. Perhaps a Word document on the left, a PDF in the upper right, and email in the lower right. It was quite tedious, and because of that I didn't do it often.

So what I'd like is 'dividers' for whole windows. And as far as I know, that didn't exist until now. I have written a script that lets you right-button drag window borders, and the script moves all neighbouring window edges at the same time. The script will grab window borders within 10 pixels (configurable) of the border/s you are right-button dragging. It also assumes a seven pixel wide window resize border, but that's also configurable at the top of the script.

The most common use I envisage having for it is running two windows side by side filling the screen. That is initially achieved by dragging the windows' title bars to opposing sides of the screen, or pressing Win+Left for one and Win+Right for the other (see BLOG LINK for more info). Then, right-button dragging the middle dividers will resize both windows!

It works with any number of window edges that align, and you can resize on a corner.
*/

ResizeStripWidth = 7 ;pixels
WindowJoinTol = 10 ;pixels
Return

~RButton::
  MouseGetPos, mX, mY, I
  StoreAllEdges()
  LX := L[I], RX := R[I], UY := U[I], DY := D[I]
  EditL := mX >= LX and mX <= LX+ResizeStripWidth
  EditR := mX <= RX and mX >= RX-ResizeStripWidth
  EditU := mY >= UY and mY <= UY+ResizeStripWidth
  EditD := mY <= DY and mY >= DY-ResizeStripWidth
  AbuttingX2 := AbuttingY2 := ""
  If (EditL) {
    diffX := mX-LX
    AbuttingX := ListAbutting(I, "L", AbuttingX2, WindowJoinTol)
  } Else {
    diffX := mX-RX
    AbuttingX := ListAbutting(I, "R", AbuttingX2, WindowJoinTol)
  }
  If (EditU) {
    diffY := mY-UY
    AbuttingY := ListAbutting(I, "U", AbuttingY2, WindowJoinTol)
  } Else {
    diffY := mY-DY
    AbuttingY := ListAbutting(I, "D", AbuttingY2, WindowJoinTol)
  }
  SetTimer, ResizeThem, 1
Return

~RButton Up::
  SetTimer, ResizeThem, Off
  Gosub, ResizeThem
Return

ResizeThem:
  ;; Move the windows
  MouseGetPos, nX, nY
  LX := L[I], RX := R[I], UY := U[I], DY := D[I]
  If (EditL) {
    MoveLeft(I, nX-diffX)
    Loop, Parse, AbuttingX, `,
      MoveRight(A_LoopField, nX-diffX)
    Loop, Parse, AbuttingX2, `,
      MoveLeft(A_LoopField, nX-diffX)
  } Else If (EditR) {
    MoveRight(I, nX-diffX)
    Loop, Parse, AbuttingX, `,
      MoveLeft(A_LoopField, nX-diffX)
    Loop, Parse, AbuttingX2, `,
      MoveRight(A_LoopField, nX-diffX)
  }
  If (EditU) {
    MoveTop(I, nY-diffY)
    Loop, Parse, AbuttingY, `,
      MoveBottom(A_LoopField, nY-diffY)
    Loop, Parse, AbuttingY2, `,
      MoveTop(A_LoopField, nY-diffY)
  } Else If (EditD) {
    MoveBottom(I, nY-diffY)
    Loop, Parse, AbuttingY, `,
      MoveTop(A_LoopField, nY-diffY)
    Loop, Parse, AbuttingY2, `,
      MoveBottom(A_LoopField, nY-diffY)
  }
Return

;; Lists window Ids abutting the WinId window on the side indicated
;; by Direction, separated by commas. Specify a tolerance to be more
;; forgiving in abuttment calculations.
ListAbutting(WinId, Direction, ByRef Others, Tol=0) {
  global L, R, U, D
  If Direction = L
    StickTo := L[WinId]
  Else If Direction = R
    StickTo := R[WinId]
  Else If Direction = U
    StickTo := U[WinId]
  Else If Direction = D
    StickTo := D[WinId]
  Else
    MsgBox, 1, Error in Window Snapper, Invalid direction passed. It must be one of L`, R`, U or D.
  For wI, UY in U
  {
    If (WinId = wI)
      Continue
    If ((Direction = "L" and Abs(StickTo-R[wI]) <= Tol)
     or (Direction = "R" and Abs(StickTo-L[wI]) <= Tol)
     or (Direction = "U" and Abs(StickTo-D[wI]) <= Tol)
     or (Direction = "D" and Abs(StickTo-U[wI]) <= Tol))
      Abutt .= (Abutt = "") ? wI : ("," . wI)
    If ((Direction = "L" and Abs(StickTo-L[wI]) <= Tol)
     or (Direction = "R" and Abs(StickTo-R[wI]) <= Tol)
     or (Direction = "U" and Abs(StickTo-U[wI]) <= Tol)
     or (Direction = "D" and Abs(StickTo-D[wI]) <= Tol))
      Others .= (Others = "") ? wI : ("," . wI)
  }
  ;ToolTip, Neighbours = "%Abutt%"
  Return Abutt
}
StoreAllEdges() {
  global U, D, L, R
  U := {}, D := {}, L := {}, R := {}
  WinGet, Id, List,,, Program Manager
  Loop, %Id%
  {
    I := Id%A_Index%
    WinGetTitle, Title, ahk_id %I%
    If Title=
      Continue
    StoreEdges(I)
  }
}

StoreEdges(WinId) {
  global L, R, U, D
  WinGetPos, x, y, w, h, ahk_id %WinId%
  U[WinId] := y
  D[WinId] := y+h
  L[WinId] := x
  R[WinId] := x+w
}

MoveLeft(WinId, newL) {
  WinGetPos, x, y, w, h, ahk_id %WinId%
  ExpectedW := x-newL+w
  WinMove, ahk_id %WinId%,, newL, y, x-newL+w, h
  WinGetPos, x, y, w, h, ahk_id %WinId%
  L[WinId] := 
  If w <> ExpectedW
    WinMove, ahk_id %WinId%,, newL+ExpectedW-w, y, w, h
  Return
}

MoveRight(WinId, newR) {
  WinGetPos, x, y, w, h, ahk_id %WinId%
  WinMove, ahk_id %WinId%,, x, y, newR-x, h
  Return
}

MoveTop(WinId, newT) {
  WinGetPos, x, y, w, h, ahk_id %WinId%
  ExpectedH := y-newT+h
  WinMove, ahk_id %WinId%,, x, newT, w, ExpectedH
  WinGetPos, x, y, w, h, ahk_id %WinId%
  If h <> ExpectedH
    WinMove, ahk_id %WinId%,, x, newT+ExpectedH-h, w, h
  Return
}

MoveBottom(WinId, newB) {
  WinGetPos, x, y, w, h, ahk_id %WinId%
  WinMove, ahk_id %WinId%,, x, y, w, newB-y
  Return
}

;; Messing around to test the functions!
#u::
  ;ActiveId := WinExist("A")
  MouseGetPos, mX, mY, mI
  ;MoveLeft(ActiveId, mX)
  le := L[mI], ri := R[mI], up := U[mI], bo := D[mI]
  MsgBox, X - %mX%`, Y - %mY% (left=%le%`, top=%up%`, right=%ri%`, bottom=%bo%)
Return
